import { expect, type Locator, type Page } from '@playwright/test'

export class NavigationBar {
    readonly page: Page;
    readonly clothesLink: Locator;
    readonly menClothesLink: Locator;
    readonly signInLink: Locator;
    readonly cartLink: Locator;

    constructor(page: Page) {
        this.page = page;
        this.signInLink = page.getByTitle("Log in to your customer account");
        this.cartLink = page.getByRole("link", { "name": "Cart"});
    }

    async goToLoginPage() {
        await this.signInLink.click();
    }

    async goToCategoryPage(category: string, subcategory: string) {
        if (subcategory) {
            await this.page.getByRole("link", { name: category, exact: true}).hover();
            await this.page.getByRole("link", { name: subcategory, exact: true}).click();
        }
        else {
            await this.page.getByRole("link", { name: category, exact: true}).click();
        }
    }

    async goToCart() {
        await this.cartLink.click();
    }
}