import { expect, type Locator, type Page } from '@playwright/test'

export class CategoryPage {
    readonly page: Page;
    readonly productLinkList: Locator;

    constructor(page: Page) {
        this.page = page;
        this.productLinkList = page.locator("//h2[contains(@class, 'product-title')]/a");
    }

    async clickOnProduct(name: string) {
        await this.productLinkList
            .filter({ hasText: name})
            .click();
    }
}