import { expect, type Locator, type Page } from '@playwright/test'

export class CartPage {
    readonly page: Page;
    readonly cartTotalPrice: Locator;
    readonly cartTotalNumber: Locator;
    readonly cartProductList: Locator;

    constructor(page: Page) {
        this.page = page;
        this.cartProductList = page.locator("//li[@class='cart-item']");
        this.cartTotalNumber = page.locator("//div[@id='cart-subtotal-products']/span[contains(@class, 'js-subtotal')]");
    }

    async checkCartItem(productList) {
        var productArray = await this.cartProductList.all();
        expect(productArray.length).toEqual(productList.length);
        for (const [index, item] of productArray.entries()) {
            expect(await item.locator("//div[@class='product-line-info']/a")).toHaveText(productList[index].name, {ignoreCase: true});
            expect(await item.locator("//input[@type='number']").getAttribute("value")).toEqual(productList[index].quantity.toString());
        }
    }

    async increaseTheFirstItemQuantity(productList, quantity) {
        var productArray = await this.cartProductList.all();
        expect(await productArray[0].locator("//div[@class='product-line-info']/a")).toHaveText(productList[0].name, {ignoreCase: true});
        await productArray[0].locator("//input[@type='number']").fill((productList[0].quantity + quantity).toString());
    }

    async checkTotalCountOfItems(quantity) {
        this.cartTotalNumber.click();
        expect(this.cartTotalNumber).toContainText(quantity + " items");
    }
}