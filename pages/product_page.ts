import { expect, type Locator, type Page } from '@playwright/test'

export class ProductPage {
    readonly page: Page;
    readonly productName: Locator;
    readonly sizeCombobox: Locator;
    readonly addToCartBtn: Locator;
    readonly quantityInput: Locator;
    readonly modalGoToCartBtn: Locator;
    readonly modalContinueShoppingBtn: Locator;
    

    constructor(page: Page) {
        this.page = page;
        this.productName = page.locator("//h1");
        this.sizeCombobox = page.getByLabel("Size");
        this.addToCartBtn = page.getByRole("button", { name: "Add to cart" });
        this.quantityInput = page.getByLabel("Quantity");
        this.modalGoToCartBtn = page.getByRole("link", { name: "Proceed to checkout" });
        this.modalContinueShoppingBtn = page.getByRole("button", { name: "Continue shopping" });
    }

    async addToCartAndContinueShopping() {
        await this.addToCartBtn.click();
        await this.modalContinueShoppingBtn.click();
    }

    async setQuantity(quantity: number) {
        await this.quantityInput.fill(quantity.toString());
    }

    async setSize(size: string) {
        await this.sizeCombobox.selectOption(size);
    }

    async goToCartFromModal() {
        await this.modalGoToCartBtn.click()
    }
}