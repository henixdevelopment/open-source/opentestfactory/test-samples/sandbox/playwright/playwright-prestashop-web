# playwright-prestashop-web

This project is used to test quickly playwright (typescript) using prestashop as the SUT.

It concerns only GUI tests and the project structure is based on the page object model.

By default the baseURL is set to http://127.0.0.1:8080, it can be overwritten by the environment variable SUT_URL.
