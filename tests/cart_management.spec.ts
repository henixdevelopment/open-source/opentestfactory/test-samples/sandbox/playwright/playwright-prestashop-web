import { test as base, expect } from "@playwright/test"
import { NavigationBar } from "../pages/navigation_bar"
import { CategoryPage } from "../pages/category_page"
import { ProductPage } from "../pages/product_page";
import { CartPage } from "../pages/cart_page";

type CartFixture = {
    navBar: NavigationBar;
    catPage: CategoryPage;
    prodPage: ProductPage;
    cartPage: CartPage;
}

export const test = base.extend<CartFixture>({
    navBar: async ({ page }, use) => {
        await use(new NavigationBar(page));
    },

    catPage: async ({ page }, use) => {
        await use(new CategoryPage(page));
    },

    prodPage: async ({ page }, use) => {
        await use(new ProductPage(page));
    },

    cartPage: async ({ page }, use) => {
        await use(new CartPage(page));
    },
})


const productList = [
    {name: "Hummingbird Printed T-Shirt", category: "Clothes", subcategory: "Men", quantity: 5},
    {name: "Mountain Fox Notebook", category: "Accessories", subcategory: "Stationery", quantity: 2},
]

test.beforeEach(async ({ page, navBar, catPage, prodPage }) => {
    await page.goto('');
    for (const product of productList) {
        await navBar.goToCategoryPage(product.category, product.subcategory);
        await catPage.clickOnProduct(product.name);
        await prodPage.setQuantity(product.quantity);
        await prodPage.addToCartAndContinueShopping();
    }
    await navBar.goToCart();
  });

test("Cart page should show a list with the correct items and quantities", async ({ cartPage }) => {
    await cartPage.checkCartItem(productList);
})

test("Updating a product's quantity should update the total number of items", async ({ cartPage }) => {
    var totalQuantity: number = 0;
    var increaseQuantity: number = 1;
    for (const product of productList) {
        totalQuantity = totalQuantity + product.quantity;
    }
    await cartPage.increaseTheFirstItemQuantity(productList, increaseQuantity);
    await cartPage.checkTotalCountOfItems(totalQuantity + increaseQuantity);
})

test("Remove a product from cart", async ({ page }) => {

})